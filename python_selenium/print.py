#print
print ("hello world")

#varible
a=1
b=2
c=3

print(a)
print(b)
print(c)


# if

if a>b:
    print("big")
elif a==b:
    print("same")
else:
   print("small")

# for

for i in range(0,10,1):
    print(i)
    
for i in (1,3,5,7): # for in tuple
    print(i)

 # while
number=0

while(number<10):
    print(number)
    number+=1



# try-catch
import sys

try:
    mm = 9 / 0
except:
    # sys.exc_info()[0] 就是用來取出except的錯誤訊息的方法
    print("Unexpected error:", sys.exc_info()[0])
finally:
    print('finally')

# function

def test_function(s):
    print(s)


test_function("call a funciton")


# get current function
print(__name__);

# dictionary
dic = {
    0:"Zero",
    1:"One"
    }

print(dic.get(0,"None"))
print(dic.get(1,"None"))
print(dic.get(2,"None"))

dic.setdefault("N")

print(dic.get(2))

# class

from obj import objd

o = objd()

o.fun1("test class")

o.deletest()

# python use /t check struct

