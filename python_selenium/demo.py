
from selenium import selenium
import time
import datetime, calendar
import subprocess
import shutil,sys
import os
import sys
import urllib
import HTMLTestRunner
import unittest

class SeleniumDemo(unittest.TestCase):
    
    def setUp(self):
        self.sel = selenium("localhost", 4444, "*firefox", "http://sat.qlync.com")
        self.sel.start()
        time.sleep(3)
        self.sel.window_maximize()
        self.sel.open("/")
        time.sleep(10)

    def test_browserLog_context(self):
        self.sel.set_browser_log_level("debug")
        print "set browser log level as debug"
        self.sel.set_context("This is Selenium Demo")
        print "add context - This is Selenium Demo"

    def test_select(self):
        self.sel.select("//*[@id='user_language-select']","index=2")
        time.sleep(3)
        print "select English language"
        index=self.sel.get_selected_index("//*[@id='user_language-select']")
        self.assertEqual("2",index)

    def test_type_text(self):
        self.sel.select("//*[@id='user_language-select']","index=2")
        time.sleep(3)
        self.sel.type("id=name-edit", "demo")
        time.sleep(1)
        self.sel.type("id=pwd-edit", "demo")
        time.sleep(1)
        print "type text to username&password column"
        value=self.sel.get_value("id=name-edit")
        self.assertEqual("demo",value)

    def test_type_keys(self):
        self.sel.select("//*[@id='user_language-select']","index=2")
        time.sleep(3)
        self.sel.type("id=name-edit", "demo")
        time.sleep(1)
        self.sel.type("id=pwd-edit", "demo")
        time.sleep(3)
        self.sel.type_keys("id=name-edit", "demo")
        time.sleep(1)
        self.sel.type_keys("id=pwd-edit", "demo")
        time.sleep(1)
        print "type keys to username&password column"
        self.sel.click("id=login-btn")
        time.sleep(15)
        keys=self.sel.get_text("//html/body/div[3]/div/div[2]/div/ul/li/a")
        self.assertEqual("Logout",keys)

    def test_uncheck(self):
        self.sel.open_window("http://192.168.11.254","sat-110")
        time.sleep(5)
        self.sel.select_pop_up("null")
        self.sel.click("link=Wireless")
        time.sleep(3)
        self.sel.click("link=Basic Settings")
        time.sleep(3)
        self.sel.check("//html/body/div/div[2]/div/form/input[2]")
        print "Disable Wireless LAN Interface Checkbox ON"
        time.sleep(3)
        self.sel.uncheck("//html/body/div/div[2]/div/form/input[2]")
        print "Disable Wireless LAN Interface Checkbox OFF"
        uncheck=self.sel.get_value("//html/body/div/div[2]/div/form/input[2]")
        self.assertEqual("off",uncheck)

    def test_remote_logs(self):
        log=self.sel.retrieve_last_remote_control_logs()
        print log
        
    def tearDown(self):
        time.sleep(3)
        self.sel.stop()

if __name__ == "__main__":
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(SeleniumDemo))
	dateTimeStamp = time.strftime('%Y%m%d_%H_%M_%S')
	buf = file("TestReport" + "_" + dateTimeStamp + ".html", 'wb')
	runner = HTMLTestRunner.HTMLTestRunner(
            stream=buf,
            title='Test the Report',
            description='Result of tests'
            )
	runner.run(suite)

        
        
        
